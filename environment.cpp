#include "environment.h"
#include "connection.h"
#include "sqlexception.h"
#include <cstring>

ogcci::Environment::Environment()
{
    connInfo = "host=127.0.0.1 port=5432";
}
ogcci::Environment::~Environment()
{
    if(o_conn != nullptr)
        delete o_conn;
}
ogcci::Environment * ogcci::Environment::createEnvironment()
{
    Environment * env = new Environment();
    return env;
};

void ogcci::Environment::terminateEnvironment(Environment * env)
{
    delete env;
};

ogcci::Connection * ogcci::Environment::createConnection(const std::string userName, 
                                           const std::string password, 
                                           const std::string connectString)
{
    if(connectString.length() == 0)
    {
        connInfo = connInfo + " dbname=postgres";
    }
    else if(connectString.find('/') == std::string::npos)
    {
        connInfo = connInfo + " dbname=" + connectString;
    }
    else 
    {
        int po1 = connectString.find(':');
        int po2 = connectString.find('/');
        connInfo = "host=";
        connInfo.append(connectString, 0, po1);
        connInfo += " port=";
        connInfo.append(connectString, po1+1, po2-po1-1);
        connInfo += " dbname=";
        connInfo.append(connectString, po2+1, connectString.length()-po2-1);
    }
    connInfo = connInfo + " user=" + userName;
    connInfo = connInfo + " password=" + password;
    _conn = PQconnectdb(connInfo.c_str());
    if (PQstatus(_conn) != CONNECTION_OK)
    {
        SQLException * e = new SQLException();
        e->setErrorCode("01");
        e->setMessage(PQerrorMessage(_conn));
        throw *e;
    }
    Connection * connection = new Connection(_conn);
    return connection;
};

void ogcci::Environment::terminateConnection(Connection * connection)
{
    if(connection != nullptr)
        delete connection;
};