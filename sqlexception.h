#ifndef __SQLEXCEPTION__
#define __SQLEXCEPTION__

#include <string>
#include "libpq-fe.h"


namespace ogcci {
    class SQLException
    {
        public:
            SQLException();
            ~SQLException();
            void setErrorCode(std::string _errorCode = "00");
            void setMessage(std::string _message = "NULL");
            std::string what();
            std::string getErrorCode();
            std::string getMessage();
        private:
            std::string errorCode;
            std::string message;
    };
}


#endif // !__SQLEXCEPTION__