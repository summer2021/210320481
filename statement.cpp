#include "statement.h"
#include "sqlexception.h"
#include "cstring"

ogcci::Statement::Statement(PGconn *_conn, std::string sql)
{
    conn = _conn;
    nParams = 0;
    setSQL(sql);
    resultSet = new ResultSet();
};

ogcci::Statement::~Statement()
{
    sqlstr = "";
    nParams = 0;
    conn = nullptr;
    resultSet = nullptr;
};

void ogcci::Statement::setSQL(std::string sql)
{
    sqlstr = sql;
    int pos = 0;
    while(sqlstr.find(':', pos)!=std::string::npos)
    {
        nParams ++;
        pos = sqlstr.find(':', pos);
        pos = pos + 1;
    }
};

void ogcci::Statement::setString(int pos, std::string val)
{
    paramValues[pos-1] = val.c_str();
};

void ogcci::Statement::setInt(int pos, int val)
{
    str[pos-1] = std::to_string(val);
    paramValues[pos-1] = str[pos-1].c_str();
};

ogcci::ResultSet * ogcci::Statement::execute(std::string sql)
{
    if(sql != "")
        setSQL(sql);
    if(nParams == 0)
        resultSet->setResultSet(PQexec(conn, sqlstr.c_str()));
    else
        resultSet->setResultSet(PQexecParams(conn, sqlstr.c_str(), nParams, NULL, paramValues, NULL, NULL, 1));
    if (PQresultStatus(resultSet->getResult()) != PGRES_COMMAND_OK)
    {
        ogcci::SQLException * e = new SQLException();
        e->setErrorCode("02");
        e->setMessage(PQresultErrorMessage(resultSet->getResult()));
        throw *e;
    }
    return resultSet;
};

void ogcci::Statement::closeResultSet(ResultSet * resultSet)
{
    delete resultSet;
};