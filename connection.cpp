#include "connection.h"
#include "environment.h"
#include "sqlexception.h"
#include "statement.h"
#include <cstring>

ogcci::Connection::Connection(PGconn *_conn)
{
    conn = _conn;
};

ogcci::Connection::~Connection()
{
        PQfinish(conn);
};

ConnStatusType ogcci::Connection::DBstatus()
{
    return PQstatus(conn);
};

ogcci::Statement * ogcci::Connection::createStatement(std::string sql)
{
    Statement * statement = new Statement(conn, sql);
    return statement;
};

void ogcci::Connection::terminateStatement(Statement * statement)
{
    delete statement;
};