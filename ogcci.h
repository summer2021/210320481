#ifndef __OGCCI__
#define __OGCCI__

#ifndef __ENVIRONMENT__
#include "environment.h"
#endif

#ifndef __CONNECTION__
#include "connection.h"
#endif

#ifndef __STATEMENT__
#include "statement.h"
#endif

#ifndef __RESULT__
#include "result.h"
#endif

#ifndef __SQLEXCEPTION__
#include "sqlexception.h"
#endif


namespace ogcci{

    class Environment;
    class Connection;
    class Statement;
    class ResultSet;
    class SQLException;

}      /* end of namespace ogcci */
#endif /*   !__OGCCI__   */