#ifndef __CONNECTION__
#define __CONNECTION__

#include <string>
#include "statement.h"
#include "libpq-fe.h"

namespace ogcci {
    class Connection
    {
        public:
            Connection(PGconn *_conn);
            ~Connection();
            ConnStatusType DBstatus();
            Statement * createStatement(std::string sql = "");
            void terminateStatement(Statement * statement);
        private:
            PGconn *conn;

    };
}


#endif // !__CONNECTION__