#ifndef __ENVIRONMENT__
#define __ENVIRONMENT__

#include <string>
#include "connection.h"
#include "libpq-fe.h"

namespace ogcci {
    
    class Environment
    {
        public:
            Environment();
            ~Environment();
            static Environment * createEnvironment();
            static void terminateEnvironment(Environment * env);
            Connection * createConnection(const std::string userName,         /* login database user      */
                                        const std::string password,         /* login database password  */
                                        const std::string connectString);   /* database information     */
            void terminateConnection(Connection * connection);
            private:
                Connection * o_conn;
                std::string connInfo;
                PGconn *_conn;
                PGresult *_res;
    };

}


#endif // !__ENVIRONMENT__