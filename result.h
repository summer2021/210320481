#ifndef __RESULT__
#define __RESULT__

#include <string>
#include "libpq-fe.h"

namespace ogcci {
    class ResultSet
    {
        public:
            ResultSet();
            ~ResultSet();
            void setResultSet(PGresult *_res);
            PGresult * getResult();
            bool next();
            std::string getString(int pos);
        private:
            PGresult *res;
            int i, j;
    };
}


#endif // !__RESULT__
