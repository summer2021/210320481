#include "sqlexception.h"
#include "cstring"

ogcci::SQLException::SQLException()
{
    setErrorCode();
    setMessage();
};

ogcci::SQLException::~SQLException()
{

};

void ogcci::SQLException::setErrorCode(std::string _errorCode)
{
    errorCode = _errorCode;
};

void ogcci::SQLException::setMessage(std::string _message)
{
    message = _message;
};

std::string ogcci::SQLException::what()
{
    std::string str = "Exception error " + errorCode + ": " + message;
    return str;
};

std::string ogcci::SQLException::getErrorCode()
{
    return errorCode;
};

std::string ogcci::SQLException::getMessage()
{
    return message;
};