#ifndef __STATEMENT__
#define __STATEMENT__

#include <string>
#include "result.h"
#include "libpq-fe.h"

namespace ogcci {
    class Statement
    {
        public:
            Statement(PGconn *_conn, std::string sql);
            ~Statement();
            void setSQL(std::string sql);
            void setString(int pos, std::string val);
            void setInt(int pos, int val);
            ResultSet * execute(std::string sql = "");
            void closeResultSet(ResultSet * resultSet);
        private:
            PGconn *conn;
            std::string sqlstr;
            ResultSet * resultSet;
            std::string str[20];
            int nParams;
            const char * paramValues[20];
            const int *paramLengths;
            const int *paramFormats;
            int resultFormat;
    };
}


#endif // !__STATEMENT__