#include <iostream>
#include "ogcci.h"
#include <stdio.h>
using namespace std;
using namespace ogcci;

int main()
{
    Environment * env;
    Connection *conn;
    Statement * stmt;
    const string name = "kirin";
    const string pass = "lablab1008@";
    const string connstr = "127.0.0.1:5432/kirindb";
    try
    {
        env = Environment::createEnvironment();
        conn = env->createConnection(name, pass, connstr);
        stmt = conn->createStatement("insert into friends values(:1, 'Ori', :2)");
        //stmt = conn->createStatement();
        //stmt->setSQL("insert into friends values(:1, 'Norman', :2)");
        stmt->setInt(1, 15);
        stmt->setString(2, "Female");
        stmt->execute();
    }
    catch(SQLException e)
    {
        cout << e.what() << endl;
    };

    conn->terminateStatement(stmt);
    env->terminateConnection(conn);
    Environment::terminateEnvironment(env);
    return 0;
}