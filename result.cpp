#include "result.h"
#include "cstring"

ogcci::ResultSet::ResultSet()
{
    i = 0;
    j = 0;
};

ogcci::ResultSet::~ResultSet()
{
    PQclear(res);
};

void ogcci::ResultSet::setResultSet(PGresult *_res)
{
    res = _res;
};

PGresult * ogcci::ResultSet::getResult()
{
    return res;
}